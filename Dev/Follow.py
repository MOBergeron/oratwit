from tkinter import *

class Follow(Frame):
	def __init__(self,parent,parentFrame,user,**options):
		Frame.__init__(self,parentFrame,options)
		self.parent = parent
		self.im = PhotoImage(file="Assets/egg.gif")
		
		self.user=user
		
		canva = Canvas(self,width=100,height=100)
		canva.create_image(50,50,image=self.im)
		canva.grid(column=0,row=0,rowspan=2,padx=5)
		
		subFrame = Frame(self)
		subFrame.grid(column=1,row=0,sticky=EW)
		subFrame.grid_columnconfigure(0,minsize=672)
		
		username = user["username"] + " - " + user["firstname"] + " " +  user["lastname"]
			
		label = Label(subFrame,text=username,font=('Arial',"9","bold"))
		label.grid(column=0,row=0,sticky=W)
		label.bind('<Button-1>',self.seeProfil)
		
		if(user["biography"] != None):
			text = Text(self,height=4,width=18)
			text.insert(1.0,user["biography"])
			text.config(state=DISABLED)
			text.grid(column=1,row=1,sticky=E+W)
	
	def seeProfil(self,event):
		self.parent.parent.showHomeProfil(self.user["id"])