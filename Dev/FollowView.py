from tkinter import * 
from Follow import *
class FollowView:
	def __init__(self,parent):
		self.parent = parent
		self.im = PhotoImage(file="Assets/egg.gif")
		
		self.followViewFrame = Canvas(self.parent.tweetContainerFrame,width=800,height=740)
		self.scrollbarY = Scrollbar(self.parent.tweetContainerFrame,command=self.followViewFrame.yview)
		self.followViewFrame.config(yscrollcommand=self.scrollbarY.set)
		
		self.scrollbarX = Scrollbar(self.parent.tweetContainerFrame,command=self.followViewFrame.xview,orient=HORIZONTAL)
		self.followViewFrame.config(xscrollcommand=self.scrollbarX.set)
		
		self.updatableFrame = Frame(self.followViewFrame)
		
	def display(self):
		self.update()
		self.scrollbarY.pack(side=RIGHT,fill=Y)
		self.scrollbarX.pack(side=BOTTOM,fill=X)
		self.followViewFrame.pack()
		
	def hide(self):
		self.followViewFrame.pack_forget()
		self.scrollbarY.pack_forget()
		self.scrollbarX.pack_forget()
		
	def update(self):
		self.clean()
		
		self.updatableFrame = Frame(self.followViewFrame)
		self.window = self.followViewFrame.create_window(0,0,window=self.updatableFrame,anchor="nw")
		
		self.listTweetFrame = Frame(self.updatableFrame)
		self.listTweetFrame.grid(column=0,row=1)
		
	def clean(self):
		self.updatableFrame.destroy()
	
	def addFollow(self,userList):
		for user in userList:
			f = Follow(self,self.listTweetFrame,user,pady=5,padx=10)
			f.pack(expand=1,fill=Y)
			
		self.listTweetFrame.update_idletasks()
		self.followViewFrame.configure(scrollregion=(0,0,self.listTweetFrame.winfo_reqwidth(),self.listTweetFrame.winfo_reqheight()+20))