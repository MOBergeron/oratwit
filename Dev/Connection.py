# -*- coding: utf-8 -*-
import cx_Oracle
from Constants import *

class Connection():
	def connect(self):
		dsn_tns = cx_Oracle.makedsn(ORACLE_SERVER, ORACLE_PORT, ORACLE_DOMAIN)
		connectionString = ORACLE_USERNAME + '/' + ORACLE_PASSWORD + '@' + dsn_tns
		self.connection = cx_Oracle.connect(connectionString)
		self.cursor = self.connection.cursor()

	def disconnect(self):
		self.cursor.close()
		self.connection.close()

	def getConnection(self):
		return self.connection, self.cursor