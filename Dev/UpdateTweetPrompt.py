# -*- coding: iso-8859-15 -*-

from tkinter import *

class UpdateTweetPrompt(Toplevel):
	def __init__(self,parent,info):
		Toplevel.__init__(self)
		
		self.parent=parent
		self.info = info
		
		self.title("Modification du tweet")
		self.minsize(325,300)
		
		fenCommande = Frame(self)
		fenCommande.pack(expand=1, fill=BOTH)
		
		self.textTweet = Text(fenCommande,height=8,width=18)
		self.textTweet.insert(1.0,info["message"])
		self.textTweet.pack(fill=BOTH)
		self.textTweet.bind("<Key>",self.checkLength)
		
		frameBut = Frame(self)
		frameBut.pack(expand=1, fill=Y)
		
		updateBut = Button(frameBut,text="Modifier",command=self.update)
		updateBut.grid(column=0,row=0,padx=40)
		cancelBut = Button(frameBut,text="Annuler",command=self.cancel)
		cancelBut.grid(column=1,row=0,padx=40)
		
	def checkLength(self,event):
		text = self.textTweet.get("1.0",END)
		if len(text) >= 139:
			self.textTweet.delete("1.0",END)
			self.textTweet.insert(1.0,text[:139])
	
	def cancel(self):
		self.destroy()
	
	def update(self):
		self.info["message"] = self.textTweet.get("1.0",END)
		#parent.parent est �gal a homeview
		self.parent.parent.updateTweet(self.info)
		self.cancel()