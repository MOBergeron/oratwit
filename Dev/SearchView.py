from tkinter import *
from Follow import *

class SearchView:
	def __init__(self,parent):
		self.parent = parent
		self.im = PhotoImage(file="Assets/egg.gif")

		self.searchViewFrame = Canvas(self.parent.tweetContainerFrame,width=850,height=740)
		self.scrollbarY = Scrollbar(self.parent.tweetContainerFrame,command=self.searchViewFrame.yview)
		self.searchViewFrame.config(yscrollcommand=self.scrollbarY.set)
		
		self.scrollbarX = Scrollbar(self.parent.tweetContainerFrame,command=self.searchViewFrame.xview,orient=HORIZONTAL)
		self.searchViewFrame.config(xscrollcommand=self.scrollbarX.set)

		self.updatableFrame = Frame(self.searchViewFrame)
	
	def display(self):
		self.update()
		self.scrollbarY.pack(side=RIGHT,fill=Y)
		self.scrollbarX.pack(side=BOTTOM,fill=X)
		self.searchViewFrame.pack(expand=1)
		
	def hide(self):
		self.searchViewFrame.pack_forget()
		self.scrollbarY.pack_forget()
		self.scrollbarX.pack_forget()
	
	def update(self):
		self.clean()
		
		self.updatableFrame = Frame(self.searchViewFrame)
		self.window = self.searchViewFrame.create_window(0,0,window=self.updatableFrame,anchor="nw")
		
		self.inputFrame = Frame(self.updatableFrame)
		self.inputFrame.pack(expand=1,fill=X)
		
		self.textSearch = Entry(self.inputFrame,bg='gray',width=50)
		self.textSearch.pack(side=LEFT,padx=20)
		
		submit = Button(self.inputFrame,command=self.search, text="Rechercher")
		submit.pack(side=LEFT)
		
	def clean(self):
		self.updatableFrame.destroy()
	
	def search(self):
		result = self.parent.searchUser(self.textSearch.get())
		self.update()
		
		self.listUserFrame = Frame(self.updatableFrame)
		self.listUserFrame.pack(expand=1,fill=BOTH)
		
		for user in result:
			f = Follow(self,self.listUserFrame,user,pady=5,padx=10)
			f.pack()
		
		self.listUserFrame.update_idletasks()
		self.searchViewFrame.configure(scrollregion=(0,0,self.listUserFrame.winfo_reqwidth(),self.listUserFrame.winfo_reqheight()+120))
		