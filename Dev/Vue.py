import LoginWindow
import TwitWindow
from tkinter import * 

class Vue():
	def __init__(self,parent):
		self.parent = parent
		self.state = "login"
		
		self.startWindow()
		self.loginWindow = LoginWindow.LoginWindow(self)
		self.twitWindow = TwitWindow.TwitWindow(self)
		
	def startWindow(self):
		self.root = Tk()
		self.root.title("Twitter login")
	
	def display(self):
		self.root.mainloop()
		
	def displayLogin(self):
		self.root.minsize(325,450)
		self.loginWindow.display()
		
	def hideLogin(self):
		self.loginWindow.hide()
		
	def displayTwit(self):
		self.root.minsize(325,570)
		self.twitWindow.display()
		
	def hideTwit(self):
		self.twitWindow.hide()