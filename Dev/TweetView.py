from tkinter import * 
from Tweet import *

class TweetView:
	def __init__(self,parent):
		self.parent = parent
		self.im = PhotoImage(file="Assets/egg.gif")

		self.tweetViewFrame = Canvas(self.parent.tweetContainerFrame,width=850,height=740)
		self.scrollbarY = Scrollbar(self.parent.tweetContainerFrame,command=self.tweetViewFrame.yview)
		self.tweetViewFrame.config(yscrollcommand=self.scrollbarY.set)
		
		self.scrollbarX = Scrollbar(self.parent.tweetContainerFrame,command=self.tweetViewFrame.xview,orient=HORIZONTAL)
		self.tweetViewFrame.config(xscrollcommand=self.scrollbarX.set)

		self.updatableFrame = Frame(self.tweetViewFrame)

	def display(self):
		self.update()
		self.scrollbarY.pack(side=RIGHT,fill=Y)
		self.scrollbarX.pack(side=BOTTOM,fill=X)
		self.tweetViewFrame.pack(expand=1)
		
	def hide(self):
		self.tweetViewFrame.pack_forget()
		self.scrollbarY.pack_forget()
		self.scrollbarX.pack_forget()
		
	def update(self):
		self.clean()
		
		self.updatableFrame = Frame(self.tweetViewFrame)
		self.window = self.tweetViewFrame.create_window(0,0,window=self.updatableFrame,anchor="nw")
		
		self.listTweetFrame = Frame(self.updatableFrame)
		self.listTweetFrame.pack(expand=1,fill=BOTH)
		
	def clean(self):
		self.updatableFrame.destroy()
	
	def addTweets(self,listTweet,user):
		id = self.parent.getId()
		for tweet in listTweet:
			t = Tweet(self,self.listTweetFrame,tweet,user,id,pady=5,padx=10)
			t.pack(expand=1,fill=Y)
			
		self.listTweetFrame.update_idletasks()
		self.tweetViewFrame.configure(scrollregion=(0,0,self.listTweetFrame.winfo_reqwidth(),self.listTweetFrame.winfo_reqheight()+20))
			
	