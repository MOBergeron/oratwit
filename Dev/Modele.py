
class Modele():
	def __init__(self,parent):
		self.parent= parent
		print("Modele created")

	def addFollowingUser(self, followingID):
		print("Added a following user")
			
	def authenticate(self, username, password):
		success = False
	
		if username == "marcomofo" and password == "a":
			success = True
		if username == "bobbybou2" and password == "a":
			success = True
		if username == "replix" and password == "a":
			success = True
		if username == "rosme" and password == "a":
			success = True
		
		if success:
			print("Connected with " + username + "(" + str(1) + ")")
		
		#return success
		return True

	def connection(self):
		print("Connected to DB")
		
	def createUser(self, firstname, lastname, username, password):
		if firstname != "" and lastname != "" and username != "" and password != "":
			print("User created")
			return True

		return False

	def deleteUser(self, id):
		print("User deleted")
	
	def updateUser(self,user):
		print("user updated")

	def disconnectUser(self):
		print("User disconnected")
		
	def disconnect(self):
		print("Disconnected to DB")
		
	def getClientMessages(self):
		liste = list()
		liste.append(dict())
		liste[0]["username"] = "MarcoMofo"
		liste[0]["message"] = "Yes sir!"
		liste[0]["date"] = "26-04-13"
		liste[0]["image"] = "image"
		
	def getUserInfo(self):
		liste = dict()
		liste["username"] = "MarcoMofo"
		liste["firstname"] = "Marc Olivier"
		liste["lastname"] = "Bergeron"
		liste["biography"] = "Ceci est ma biographie"
		liste["website"] = "http://www.string-emil.de"
		liste["age"] = "21"
		liste["country"] = "Canada"
		liste["email"] = "emil@sup.son"
		liste["profile_picture"] = "image"
		liste["sex"] = "H"

		return liste

	def getFollowingMessages(self):
		liste = list()
		liste.append(dict())
		liste[0]["username"] = "Replix"
		liste[0]["firstname"] = "Samuel"
		liste[0]["lastname"] = "Ryc"
		liste[0]["message"] = "Sup son emil."
		liste[0]["date"] = "26-04-13"
		liste[0]["profile_picture"] = "image"

		liste.append(dict())
		liste[1]["username"] = "Rosme"
		liste[1]["firstname"] = "js"
		liste[1]["lastname"] = "f"
		liste[1]["message"] = "Hello world!"
		liste[1]["date"] = "26-04-13"
		liste[1]["profile_picture"] = "image"
		
		liste.append(dict())
		liste[2]["username"] = "Bobbybou2"
		liste[2]["firstname"] = "M"
		liste[2]["lastname"] = "G"
		liste[2]["message"] = "yo!"
		liste[2]["date"] = "26-04-13"
		liste[2]["profile_picture"] = "image"
		
		liste.append(dict())
		liste[3]["username"] = "MarcoMofo"
		liste[3]["firstname"] = "M"
		liste[3]["lastname"] = "G"
		liste[3]["message"] = "yo!"
		liste[3]["date"] = "26-04-13"
		liste[3]["profile_picture"] = "image"
		
		liste.append(dict())
		liste[4]["username"] = "Bobbybou2"
		liste[4]["firstname"] = "M"
		liste[4]["lastname"] = "G"
		liste[4]["message"] = "yo!"
		liste[4]["date"] = "26-04-13"
		liste[4]["profile_picture"] = "image"
		
		return liste
	
	def getFollowerUsername(self):
		liste = list()
		liste.append(dict())
		liste[0]["username"] = "Replix"
		liste[0]["firstname"] = "Samuel"
		liste[0]["lastname"] = "Ryc"
		liste[0]["biography"] = "biography"
		liste[0]["image"] = "image"

		liste.append(dict())
		liste[1]["username"] = "Bobbybou2"
		liste[1]["firstname"] = "Marc"
		liste[1]["lastname"] = "Grenier"
		liste[1]["biography"] = "biography"
		liste[1]["image"] = "image"

		return liste
		
	def getFollowingUsername(self):
		liste = list()
		liste.append(dict())
		liste[0]["username"] = "Replix"
		liste[0]["firstname"] = "Samuel"
		liste[0]["lastname"] = "Ryc"
		liste[0]["biography"] = "biography"
		liste[0]["image"] = "image"

		liste.append(dict())
		liste[1]["username"] = "Rosme"
		liste[1]["firstname"] = "Jean-Sébastien"
		liste[1]["lastname"] = "Fauteux"
		liste[1]["biography"] = "biography"
		liste[1]["image"] = "image"

		liste.append(dict())
		liste[2]["username"] = "Bobbybou2"
		liste[2]["firstname"] = "Marc"
		liste[2]["lastname"] = "Grenier"
		liste[2]["biography"] = "biography"
		liste[2]["image"] = "image"

		return liste
		
	def sendReply(self, twitID, msg):
		print("send reply")
		
	def sendTweet(self, msg):
		print("send twit")
		
		
