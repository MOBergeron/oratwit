CREATE UNIQUE INDEX idx_twit_username ON twitusers(username);
CREATE INDEX idx_twit_fullname ON twitusers(first_name,last_name);