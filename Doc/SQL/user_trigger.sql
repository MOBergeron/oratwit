--Creating user id trigger
CREATE OR REPLACE
TRIGGER twit_usersid_trig
BEFORE INSERT on TwitUsers
FOR EACH ROW

BEGIN
	SELECT twit_seq_userid.NEXTVAL
	INTO :new.ID
	FROM dual;
END;