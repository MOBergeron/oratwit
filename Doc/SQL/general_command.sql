--Adding a user
INSERT INTO twitusers(username, first_name, last_name, password) VALUES('username', 'prenom', 'nom_famille', 'mdp');
commit;

--Retriving info of a user
SELECT * FROM twitusers WHERE id = (SELECT id FROM twitusers WHERE username = 'macha');

--Tweeting a message
INSERT INTO twitmessage(id_user, message, publication_date) VALUES(4, 'Mon premier tweet est merveilleux!', to_date('2013/03/28 5:10:20', 'yyyy/mm/dd hh24:mi:ss'));
commit;
--Tweeting a reply
INSERT INTO twitmessage(id_user, message, publication_date, id_reply) VALUES(5, 'Le miens est mieux', to_date('2013/03/28 5:10:20', 'yyyy/mm/dd hh24:mi:ss'), 4);
commit;

--Adding a follower/following someone
INSERT INTO twitfollow(id_follower, id_following) VALUES(4, (SELECT id FROM twitusers WHERE username = 'user2'));
commit;
INSERT INTO twitfollow(id_follower, id_following) VALUES(5, (SELECT id FROM twitusers WHERE username = 'user1'));
INSERT INTO twitfollow(id_follower, id_following) VALUES(5, (SELECT id FROM twitusers WHERE username = 'user3'));
INSERT INTO twitfollow(id_follower, id_following) VALUES(8, (SELECT id FROM twitusers WHERE username = 'user1'));
commit;

--Getting the names of those that we follow
SELECT DISTINCT tu2.username, tu2.first_name, tu2.last_name, tu2.biography, tu2.profile_picture
    FROM twitusers tu1, twitusers tu2, twitfollow tf 
    WHERE tu1.id = tf.id_follower AND tu2.id = tf.id_following
    AND tf.id_follower != tf.id_following
    AND tu1.id = (SELECT id FROM twitusers WHERE username = 'macha');
    
--Getting the names of those that follow us
SELECT DISTINCT tu2.username, tu2.first_name, tu2.last_name, tu2.biography, tu2.profile_picture
    FROM twitusers tu1, twitusers tu2, twitfollow tf
    WHERE tu1.id = (SELECT id FROM twitusers WHERE username = 'macha')
    AND tu1.id = tf.id_following AND tu2.id = tf.id_follower
    AND tf.id_follower != tf.id_following;
    
--Deleting a user
DELETE FROM twitusers WHERE id = 9;
commit;

--Getting the list of message from those we follow (test here macha)
SELECT DISTINCT tu2.username, tu2.first_name, tu2.last_name, tm.id, tm.message, tm.publication_date
    FROM twitusers tu1, twitusers tu2, twitfollow tf, twitmessage tm
    WHERE tu1.id = tf.id_follower AND tu2.id = tf.id_following
    AND tu1.id = (SELECT id FROM twitusers WHERE username = 'macha')
    AND tu2.id = tm.id_user
    AND tu2.id != tu1.id
    ORDER BY tm.publication_date DESC;
    
--Getting the tweets of a user (test here macha)
SELECT DISTINCT tm.id, tm.message, tm.publication_date
    FROM twitusers tu, twitmessage tm
    WHERE tu.id = tm.id_user
    AND tu.id = (SELECT id FROM twitusers WHERE username = 'macha')
    ORDER BY tm.publication_date DESC;
    
--Updating the info of a user (test here user2)
UPDATE twitusers
    SET first_name='U4',last_name='U4',biography='THE BIO',SEX=null,
        age=22,website=null,email=null,country=null,profile_picture=null
    WHERE id = (SELECT id FROM twitusers WHERE username = 'macha');
commit;

--Getting the tweets of those that someone follow, and ours at the same time
SELECT DISTINCT tu2.username, tu2.first_name, tu2.last_name, tm.id, tm.message, tm.publication_date
    FROM twitusers tu1, twitusers tu2, twitfollow tf, twitmessage tm
    WHERE tu1.id = tf.id_follower AND tu2.id = tf.id_following
    AND tu1.id = (SELECT id FROM twitusers WHERE username = 'macha')
    AND tu2.id = tm.id_user
    ORDER BY tm.publication_date DESC;
    
--Searching for a user
SELECT DISTINCT tu.id, tu.username, tu.first_name, tu.last_name
    FROM twitusers tu
    WHERE upper(username) LIKE upper('%C%')
    OR upper(first_name) LIKE upper('%C%') 
    OR upper(last_name) LIKE upper('%C%');
    
--Delete a Tweet (Twit #5 here)
DELETE FROM TWITMESSAGE
    WHERE id = 5;
    
--Editing a tweet (Twit #5 here)
UPDATE TWITMESSAGE 
    SET message='New Message' 
    WHERE id = 5;